# Final Project

- Score: 23.5 / 26

## General

## Thor

## Spidey

- 1.00  Mime-type failures

## Demonstration

- 0.5   Needed to include larger files (1GB) for throughput
- 1.0   Scripts are great, but should include visual graphs / diagrams

## Guru Points

+ 1.00  VPS
+ 1.00  Thumbnail
